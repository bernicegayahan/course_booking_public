// console.log("hello from JS file"); 

//lets target first our form component and place it inside a new variable
let registerForm = document.querySelector("#registerUser")

//inside the first param of the method describe the event that it will listen to, while inside the 2nd parameter lets describe the action/ procedure that will happen upon triggering the said event. 
registerForm.addEventListener("submit", (pangyayari) => {
  pangyayari.preventDefault() //to avoid page refresh/page redirection once that the said event has been triggered. 

  //capture each values inside the input fields first, then repackage them inside a new variable
  let fName = document.querySelector("#firstName").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(firstName)
  let lastName = document.querySelector("#lastName").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(lastName)
  let email = document.querySelector("#userEmail").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(email)
  let mobileNo = document.querySelector("#mobileNumber").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(mobileNo)
  let password = document.querySelector("#password1").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(password)
  let verifyPassword = document.querySelector("#password2").value
  //lets create a checker to make sure that we are successful in captring the values.
  //console.log(verifyPassword)



  if (password !== verifyPassword) {
    Swal.fire({
      icon: "error",
      text: "Uh oh, passwords did not match :("
    })
  }

  else if (password.length < 8) {
    Swal.fire({
      icon: "error",
      text: "Password must be be 8-15 characters."
    });
  }

  else if (password.length > 15) {
    Swal.fire({
      icon: "error",
      text: "Password must be within 8-15 characters."
    });
  }

  else if (password.search(/[0-9]/) < 0) {
    Swal.fire({
      icon: "error",
      text: "Password must have 1 number."
    });
  }

  else if (password.search(/[A-Z]/) < 0) {
    Swal.fire({
      icon: "error",
      text: "Password must have 1 capital letter."
    });
  }

  else if (password.search(/[a-z]/) < 0) {
    Swal.fire({
      icon: "error",
      text: "Password must have 1 small letter."
    });
  }

  else if (password.search(/[!@#$%^&*]/) < 0) {
    Swal.fire({
      icon: "error",
      text: "Password must have 1 special character."
    });
  }

  else if ((fName !== "" && lastName !== "" && email !== "" && password !== "" && verifyPassword !== "") && (password === verifyPassword) && (mobileNo.length === 11)) {
    // before you allow the user to create a new account check if the email value is still available for use. This will ensure that each user will have their unique user email.
    fetch('https://dry-bastion-13688.herokuapp.com/api/users/email-exists', {
      //mode: 'no-cors',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    }).then(res => {
      return res.json() /*to make it readable once the response returns to the client side*/
    }).then(convertedData => {


      if (convertedData === false) {

        fetch('https://dry-bastion-13688.herokuapp.com/api/users/register', {
          //we will now describe the structure of our request for register
          //mode: 'no-cors',
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          //API only accepts request in a string format. 
          body: JSON.stringify({
            firstName: fName,
            lastName: lastName,
            email: email,
            mobileNo: mobileNo,
            password: password
          })
        }).then(res => {

          console.log(res)
          //console.log("hello");
          return res.json()
        }).then(data => {
          console.log(data)
          //lets create here a control structure to give out a proper response depending on the return from the backend. 
          if (data === true) {
            Swal.fire(
              'Great!',
              'You have registered successfully!',
              'success'
            )
          } else {
            //inform the user that something went wrong
            Swal.fire("Oh dear! Something went wrong during processing :(")
          }
        })
      } else {
        Swal.fire({
          icon: "error",
          text: "Email already exists!"
        })
      }
    })

    //this block of code will run if the condition has been met
    //how can we create a new account for user using the data that he/she entered?
    //url -> describes the destination of the request. 
    //3 STATES for a promise (pending, fullfillment, rejected)


  } else {
    Swal.fire("Oh dear, kindly start again :(")
  }
})

